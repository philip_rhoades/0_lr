---
layout: page
title: Frequently Asked Questions
class: 'post'
navigation: True
logo: 'assets/images/logo.png'
current: faq
---

- Isn't it "un-natural" to want to live longer than "normal"?:

No. More later . .

- Isn't there a contradiction about complaining about overpopulation and wanting longer life spans?:

No. More later . .

- Won't technology solve all the worlds problems?:<br>

No. More later . .

- Isn't trying to extend a healthy life span beyond what is "natural" pie-in-the-sky?:

No but it will require interventions with current and developing medical technology - just in the last few years there have been many developments, as well as life-style changes that can improve a person's health and life expectancy.





