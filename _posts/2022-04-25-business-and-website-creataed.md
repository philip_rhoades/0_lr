---
layout: post
cover: false
title: The Business Name, Domain Registered and Website Created
date: 2022-04-25 14:50:00
tags: news
subclass: 'post tag-campaign'
categories: 'News'
navigation: True
logo: 'assets/images/logo.png'
---

As another project situated in the "Koura Rocks" Life Extension Village in Cowra, Longevity Results will start business by offering an Epigenetic DNA Age Test.  This is a baseline test that is necessary for being able to monitor the effects of various medical interventions that should have health and longevity effects over a period of time.



